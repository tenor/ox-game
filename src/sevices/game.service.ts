import { Injectable } from '@angular/core';

import { Cell, COLUMN_COUNT, ROW_COUNT, CellValueType, getCellIndex, CellInterface } from '../app/cell/cell.model';
import { BotService } from './bot.service';
import { PlaygroundService } from './playground.service';
import { LoggerService } from './logger.service';

export const PLAYERS = ['Bot', 'You'];
const GAME_STAGE_MESSAGES = ['Bot\'s turn', 'Your turn', 'You lose...', 'You win!', 'Draw'];

@Injectable({
  providedIn: 'root'
})
export class GameService {
  isGameStarted: boolean;
  isGameOver: boolean;
  showGameOver: boolean;
  firstPlayer: number;
  currentPlayer: number;
  winner: number;
  gameStageMessage: string;

  constructor(
    public bot: BotService,
    public logger: LoggerService,
    public playground: PlaygroundService
  ) {

  }

  startNewGame(): void {
    this.playground.initFill();
    this.playground.freeCellsNumber = COLUMN_COUNT * ROW_COUNT;
    this.bot.resetGame();
    this.winner = -1;
    this.firstPlayer = Math.floor(Math.random() * 2);
    this.currentPlayer = this.firstPlayer;
    this.isGameStarted = true;
    this.isGameOver = false;
    this.showGameOver = false;
    this.gameStageMessage = GAME_STAGE_MESSAGES[this.currentPlayer];
    if (this.currentPlayer === 0) {
      this.botMove();
    }
  }

  move(row: number, column: number, value: CellValueType) {
    this.playground.markCell(row, column, value);
    if (this.checkGameOver(row, column, value)) {
      this.isGameOver = true;
      this.showGameOver = true;
      this.gameStageMessage = this.winner >= 0 ? GAME_STAGE_MESSAGES[this.winner + 2] : GAME_STAGE_MESSAGES[4];
      this.logger.addRecord(row, column, value, PLAYERS[this.currentPlayer], this.gameStageMessage);
      return;
    }
    this.logger.addRecord(row, column, value, PLAYERS[this.currentPlayer]);
    this.currentPlayer = 1 - this.currentPlayer;
    this.gameStageMessage = GAME_STAGE_MESSAGES[this.currentPlayer];
  }

  nextTurn(row: number, column: number, value: CellValueType): void {
    this.move(row, column, value);
    if (this.currentPlayer === 0) {
      this.botMove({row, column, value});
    }
  }

  botMove(cell?: Cell) {
    const moveCell: CellInterface = this.bot.makeMove(cell);
    this.move(moveCell.row, moveCell.column, moveCell.value);
  }

  checkGameOver(row: number, col: number, value: CellValueType): boolean {
    if (this.playground.checkLine(row, col, value,
      this.playground.checkLeft.bind(this.playground),
      this.playground.checkRight.bind(this.playground)) > 2
    || this.playground.checkLine(row, col, value,
      this.playground.checkUp.bind(this.playground),
      this.playground.checkDown.bind(this.playground)) > 2
    || this.playground.checkLine(row, col, value,
      this.playground.checkLeftUp.bind(this.playground),
      this.playground.checkRightDown.bind(this.playground)) > 2
    || this.playground.checkLine(row, col, value,
      this.playground.checkLeftDown.bind(this.playground),
      this.playground.checkRightUp.bind(this.playground)) > 2) {
        this.winner = this.currentPlayer;
      }
    return this.winner > -1 || this.playground.freeCellsNumber < 1;
  }

  getCellValue(row: number, column: number): CellValueType {
    const index = getCellIndex(row, column);
    return this.playground.cells[index].value;
  }

  getCells(): Cell[] {
    return [...this.playground.cells];
  }

}
