import { Injectable } from '@angular/core';

import { Cell, getCellIndex, CellValueType, COLUMN_COUNT, ROW_COUNT } from '../app/cell/cell.model';

@Injectable({
  providedIn: 'root'
})
export class PlaygroundService {
  public cells: Cell[];

  freeCellsNumber: number;

  constructor() { }

  markCell(row: number, column: number, value: CellValueType = 'X'): void {
    const index = getCellIndex(row, column);
    this.cells[index].value = value;
    this.freeCellsNumber--;
  }

  getCells(): Cell[] {
    return [...this.cells];
  }

  checkLine(
    row: number, col: number, value: CellValueType,
    less: (row: number, col: number, value: CellValueType) => number,
    more: (row: number, col: number, value: CellValueType) => number
  ): number {
    let maxChainLength = 1;
    maxChainLength += less(row, col, value);
    if (maxChainLength === 3) {
      return maxChainLength;
    }
    maxChainLength += more(row, col, value);
    return maxChainLength;
  }

  checkLeft(row: number, col: number, value: CellValueType): number {
    let result = 0;
    if (col === 0) {
      return result;
    }
    const leftIndex = getCellIndex(row, col - 1);
    if (this.cells[leftIndex].value === value) {
      result++;
      result += this.checkLeft(row, col - 1, value);
    }
    return result;
  }

  checkRight(row: number, col: number, value: CellValueType): number {
    let result = 0;
    if (col === COLUMN_COUNT - 1) {
      return result;
    }
    const rightIndex = getCellIndex(row, col + 1);
    if (this.cells[rightIndex].value === value) {
      result++;
      result += this.checkRight(row, col + 1, value);
    }
    return result;
  }


  checkUp(row: number, col: number, value: CellValueType): number {
    let result = 0;
    if (row === 0) {
      return result;
    }
    const topIndex = getCellIndex(row - 1, col);
    if (this.cells[topIndex].value === value) {
      result++;
      result += this.checkUp(row - 1, col, value);
    }
    return result;
  }

  checkDown(row: number, col: number, value: CellValueType): number {
    let result = 0;
    if (row === ROW_COUNT - 1) {
      return result;
    }
    const bottomIndex = getCellIndex(row + 1, col);
    if (this.cells[bottomIndex].value === value) {
      result++;
      result += this.checkDown(row + 1, col, value);
    }
    return result;
  }

  checkLeftUp(row: number, col: number, value: CellValueType): number {
    let result = 0;
    if (col === 0 || row === 0) {
      return result;
    }
    const leftIndex = getCellIndex(row - 1, col - 1);
    if (this.cells[leftIndex].value === value) {
      result++;
      result += this.checkLeftUp(row - 1, col - 1, value);
    }
    return result;
  }

  checkRightDown(row: number, col: number, value: CellValueType): number {
    let result = 0;
    if (col >= COLUMN_COUNT - 1 || row >= ROW_COUNT - 1) {
      return result;
    }
    const rightIndex = getCellIndex(row + 1, col + 1);
    if (this.cells[rightIndex].value === value) {
      result++;
      result += this.checkRightDown(row + 1, col + 1, value);
    }
    return result;
  }

  checkLeftDown(row: number, col: number, value: CellValueType): number {
    let result = 0;
    if (col === 0 || row >= ROW_COUNT - 1) {
      return result;
    }
    const leftIndex = getCellIndex(row + 1, col - 1);
    if (this.cells[leftIndex].value === value) {
      result++;
      result += this.checkLeftDown(row + 1, col - 1, value);
    }
    return result;
  }

  checkRightUp(row: number, col: number, value: CellValueType): number {
    let result = 0;
    if (col >= COLUMN_COUNT - 1 || row === 0) {
      return result;
    }
    const rightIndex = getCellIndex(row - 1, col + 1);
    if (this.cells[rightIndex].value === value) {
      result++;
      result += this.checkRightUp(row - 1, col + 1, value);
    }
    return result;
  }

  initFill() {
    this.cells = [];
    for (let row = 0; row < ROW_COUNT; row++) {
      for (let column = 0; column < COLUMN_COUNT; column++) {
        const cell = new Cell(row, column);
        this.cells.push(cell);
      }
    }
  }
}
