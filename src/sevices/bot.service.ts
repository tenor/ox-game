import { Injectable } from '@angular/core';

import { Cell, CellInterface, CellValueType, COLUMN_COUNT, getCellIndex, ROW_COUNT } from '../app/cell/cell.model';
import { PlaygroundService } from './playground.service';

interface ValueAssesmentInterface {
  myCount: number;
  partnersCount: number;
  emptyCount: number;
}

class ValueAssesment implements ValueAssesmentInterface {
  myCount: number;
  partnersCount: number;
  emptyCount: number;

  constructor() {
    this.myCount = 0;
    this.partnersCount = 0;
    this.emptyCount = 0;
  }

  addValues(values: ValueAssesment): void {
    this.myCount += values.myCount;
    this.partnersCount += values.partnersCount;
    this.emptyCount += values.emptyCount;
  }

  subtractValues(values: ValueAssesment): void {
    this.myCount -= values.myCount;
    this.partnersCount -= values.partnersCount;
    this.emptyCount -= values.emptyCount;
  }
}

interface MoveInterface extends CellInterface {
  lineValues: ValueAssesment[];
  maxMyLineValue: number;
  maxPartnerValue: number;
  summaryValue: number;
}

class Move extends Cell implements MoveInterface {
  lineValues: ValueAssesment[];
  maxMyLineValue: number;
  maxPartnerValue: number;
  summaryValue: number;

  constructor(row: number, column: number, value?: CellValueType) {
    super(row, column, value);
    this.lineValues = [];
    let i: number;
    for (i = 0; i < 4; i++) {
      const valueAssesmant = new ValueAssesment();
      this.lineValues.push(valueAssesmant);
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class BotService {
  cells: Cell[];
  moves: Move[];
  canWinMove: Move;
  mySign: CellValueType;
  partnerSign: CellValueType;
  isFirstMove = true;

  lastPartnersMove: Cell;

  constructor(public playground: PlaygroundService) { }

  resetGame() {
    this.isFirstMove = true;
  }

  initMoves() {
    this.mySign = 'O';
    this.partnerSign = 'X';
    this.moves = [];
    this.cells = this.playground.getCells();
    for (const cell of this.cells) {
      if (cell.value) {
        continue;
      }
      const move = new Move(cell.row, cell.column);
      this.moves.push(move);
    }
  }

  recalculateMoves(cell?: CellInterface): Move {
    if (cell) {
      const index = this.moves.findIndex((move) => move.row === cell.row && move.column === cell.column);
      if (index >= 0 ) {
        this.moves.splice(index, 1);
      }
    }
    let bestMove: Move = null;
    for (const move of this.moves) {
      this.checkMoveValues(move);
      if (this.canWinMove) {
        bestMove = this.canWinMove;
        this.canWinMove = null;

        return bestMove;
      }
      if (!bestMove || bestMove.summaryValue < move.summaryValue) {
        bestMove = move;
      }
    }
    bestMove.value = this.mySign;

    const index2 = this.moves.findIndex((move) => move.row === bestMove.row && move.column === bestMove.column);
    if (index2 >= 0) {
      this.moves.splice(index2, 1);
    }

    return bestMove;
  }

  checkMoveValues(move: Move): void {
    const index = getCellIndex(move.row, move.column);
    move.lineValues[0] = this.checkLine(
      move.row, move.column, this.checkLeftValues.bind(this), this.checkRightValues.bind(this)
    );
    move.lineValues[1] = this.checkLine(
      move.row, move.column, this.checkUpValues.bind(this), this.checkDownValues.bind(this)
    );
    move.lineValues[2] = this.checkLine(
      move.row, move.column, this.checkLeftUpValues.bind(this), this.checkRightDownValues.bind(this)
    );
    move.lineValues[3] = this.checkLine(
      move.row, move.column, this.checkLeftDownValues.bind(this), this.checkRightUpValues.bind(this)
    );

    move.maxMyLineValue = 0;
    move.maxPartnerValue = 0;
    move.summaryValue = 0;
    for (const lineValue of move.lineValues) {
      move.maxMyLineValue = Math.max(lineValue.emptyCount + lineValue.myCount * 2, move.maxMyLineValue);
      if (move.maxMyLineValue >= 5) {
        this.canWinMove = new Move(move.row, move.column, this.mySign);
        return;
      }
      move.maxPartnerValue = Math.max(lineValue.emptyCount + lineValue.partnersCount * 2, move.maxPartnerValue);
      move.summaryValue += lineValue.emptyCount + lineValue.myCount * 2;
      if (move.maxPartnerValue > 4) {
        move.summaryValue += 30;
      }
    }
  }

  checkLine(
    row: number, col: number,
    less: (row: number, col: number) => ValueAssesment,
    more: (row: number, col: number) => ValueAssesment
  ): ValueAssesment {
    const result = new ValueAssesment();
    const lessResult = less(row, col);
    result.addValues(lessResult);
    const moreResult = more(row, col);
    result.addValues(moreResult);
    const cellResult = this.checkCell(row, col);
    result.subtractValues(cellResult);

    return result;
  }

  checkCell(row: number, col: number): ValueAssesment {
    const index = getCellIndex(row, col);
    const result = new ValueAssesment();
    switch (this.cells[index].value) {
      case this.mySign : result.myCount++; break;
      case this.partnerSign: result.partnersCount++; break;
      default: result.emptyCount++;
    }
    return result;
  }

  checkLeftValues(row: number, col: number): ValueAssesment {
    const result = this.checkCell(row, col);
    if (col <= 0) {
      return result;
    }
    const sideResult = this.checkLeftValues(row, col - 1);
    result.addValues(sideResult);
    return result;
  }

  checkRightValues(row: number, col: number): ValueAssesment {
    const result = this.checkCell(row, col);
    if (col >= COLUMN_COUNT - 1) {
      return result;
    }
    const sideResult = this.checkRightValues(row, col + 1);
    result.addValues(sideResult);
    return result;
  }

  checkUpValues(row: number, col: number): ValueAssesment {
    const result = this.checkCell(row, col);
    if (row <= 0) {
      return result;
    }
    const sideResult = this.checkUpValues(row - 1, col);
    result.addValues(sideResult);
    return result;
  }

  checkDownValues(row: number, col: number): ValueAssesment {
    const result = this.checkCell(row, col);
    if (row >= ROW_COUNT - 1) {
      return result;
    }
    const sideResult = this.checkDownValues(row + 1, col);
    result.addValues(sideResult);
    return result;
  }

  checkLeftUpValues(row: number, col: number): ValueAssesment {
    const result = this.checkCell(row, col);
    if (row <= 0 || col <= 0) {
      return result;
    }
    const sideResult = this.checkLeftUpValues(row - 1, col - 1);
    result.addValues(sideResult);
    return result;
  }

  checkRightDownValues(row: number, col: number): ValueAssesment {
    const result = this.checkCell(row, col);
    if (row >= ROW_COUNT - 1 || col >= COLUMN_COUNT - 1) {
      return result;
    }
    const sideResult = this.checkRightDownValues(row + 1, col + 1);
    result.addValues(sideResult);
    return result;
  }

  checkLeftDownValues(row: number, col: number): ValueAssesment {
    const result = this.checkCell(row, col);
    if (row >= ROW_COUNT - 1 || col <= 0) {
      return result;
    }
    const sideResult = this.checkLeftDownValues(row + 1, col - 1);
    result.addValues(sideResult);
    return result;
  }

  checkRightUpValues(row: number, col: number): ValueAssesment {
    const result = this.checkCell(row, col);
    if (row <= 0 || col >= COLUMN_COUNT - 1) {
      return result;
    }
    const sideResult = this.checkRightUpValues(row - 1, col + 1);
    result.addValues(sideResult);
    return result;
  }

  setPartnersLastMove(cell: Cell): void {
    this.lastPartnersMove = cell;
  }

  makeMove(cell?: Cell): CellInterface {
    this.cells = this.playground.getCells();
    if (this.isFirstMove) {
      this.initMoves();
      this.isFirstMove = false;
    }
    return this.recalculateMoves(cell);
  }
}
