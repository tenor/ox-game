import { Injectable } from '@angular/core';

import { CellValueType } from '../app/cell/cell.model';

interface LogRecordInterface {
  time: string;
  author: string;
  row: number;
  column: number;
  value: CellValueType;
  result: string;
}

class LogRecord implements LogRecordInterface {
  time: string;
  author: string;
  row: number;
  column: number;
  value: CellValueType;
  result: string;

  constructor(row: number, column: number, value: CellValueType, author = 'You', result = '') {
    this.time = new Date().toTimeString().substring(0, 5);
    this.author = author;
    this.row = row;
    this.column = column;
    this.value = value;
    this.result = result;
  }
}

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  private readonly log: LogRecord[] = [];

  constructor() { }

  addRecord(row: number, column: number, value: CellValueType, author: string, result = ''): void {
    const record = new LogRecord(row, column, value, author, result);
    this.log.push(record);
  }

  getRecords(): string[] {
    const records = [];
    for (const record of this.log) {
      const recordString = `${record.time} ${record.author} Row${record.row} Col${record.column}`;
      records.push(recordString);
      if (record.result) {
        records.push(record.result);
      }
    }
    return records;
  }

}
