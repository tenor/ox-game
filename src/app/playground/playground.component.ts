import { Component, OnInit } from '@angular/core';

import { Cell } from '../cell/cell.model';
import { LoggerService } from '../../sevices/logger.service';
import { GameService } from '../../sevices/game.service';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.css']
})
export class PlaygroundComponent implements OnInit {
  cells: Cell[] = [];
  isLogShowed = false;

  constructor(
    public logger: LoggerService,
    public game: GameService
  ) {  }

  ngOnInit() {
    this.onRestart();
  }

  onRestart() {
    this.game.startNewGame();
    this.cells = this.game.getCells();
  }
}
