export type CellValueType = 'O' | 'X';
export const ROW_COUNT = 3;
export const COLUMN_COUNT = 3;

export interface CellInterface {
  row: number;
  column: number;
  index?: number;
  value: CellValueType;
}

export const getCellIndex = (row: number, column: number): number => {
  return row * COLUMN_COUNT + column;
};

export class Cell implements CellInterface {
  row: number;
  column: number;
  index?: number;
  value: CellValueType;

  constructor(row: number, column: number, value?: CellValueType) {
    this.row = row;
    this.column = column;
    this.value = value;
  }
}
