import { Component, Input } from '@angular/core';

import { CellValueType } from './cell.model';
import { GameService } from '../../sevices/game.service';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent {
  @Input() row: number;
  @Input() column: number;
  @Input() value: CellValueType;

  constructor(
    private game: GameService
  ) {  }

  markCell(value: CellValueType = 'X') {
    this.game.nextTurn(this.row, this.column, value);
  }

}
